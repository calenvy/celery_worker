"""
Celery settings - see http://celeryq.org/docs/configuration.html
Because celery_worker is not a Django project, there is no settings.py file.
We keep all our settings.py-like settings in this file instead.
"""

from detect_server_type import server_type

import sys, os

# Some generic, project-wide settings 

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))

LOG_ENABLED = True
LOG_FILE = sys.__stdout__       # Default logging is just to stdout,
                                #  each task will override this as necessary
LOG_DIR = os.path.normpath(os.path.join(PROJECT_DIR, os.pardir, 'logs'))
WORDLIST_DIR = os.path.join(PROJECT_DIR, 'data')
                                
# Celery settings

if server_type == 'live': 
    REDIS_HOST = BROKER_HOST = '10.176.32.205'
else:
    REDIS_HOST = BROKER_HOST = 'localhost'

CARROT_BACKEND = "ghettoq.taproot.Redis"        # use Redis as the Celery backend    
REDIS_PORT = BROKER_PORT = 6379
REDIS_DB = BROKER_VHOST = '/'
CELERY_RESULT_BACKEND = 'redis'
CELERY_IMPORTS = ('crawl.tasks',)
CELERY_CONCURRENCY = 8                          # Stick with default value for now
REDIS_PASSWORD = BROKER_PASSWORD = 'DKu47738d&#&@*dDSsjj$$$$'
CELERY_SEND_EVENTS = True       # for use with Celerymon

# TODO
#CELERYD_MAX_TASKS_PER_CHILD
#CELERYD_TASK_TIME_LIMIT
#CELERYD_SOFT_TASK_TIME_LIMIT

# All tasks in this project use the "celery_worker" queue, to keep
#  our tasks separate from Noted or any other project using Celery
# See http://celeryproject.org/docs/userguide/routing.html#routers

CELERY_DEFAULT_QUEUE = 'celery_worker'

class MyRouter(object):
    "All tasks in this Django project are in the celery_worker queue"
    
    @staticmethod
    def route_for_task(task, args=None, kwargs=None):
        return {'queue': CELERY_DEFAULT_QUEUE}
    
CELERY_ROUTES = (MyRouter, )

CELERY_QUEUES = {
    CELERY_DEFAULT_QUEUE: {
        'exchange': CELERY_DEFAULT_QUEUE
    }
}

MOSSO_API_USERNAME = 'noted'
MOSSO_API_KEY = '840c394e94da907451c72c9567dfd144'
MOSSO_USE_SERVICENET = False        # whether to use internal IP when connecting to Cloud Files

