"Utils for dealing with emails"

import email, re, email.header
import html2plaintext
from utils import *

RE_EMAIL = re.compile(r"\b[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)*([A-Za-z-]{2,})\b")
RE_URL = re.compile(r"(?i)(http://)?([A-Za-z0-9\-]+\.)+([A-Za-z]{2,6})([/?][A-Za-z0-9\-/?%=&.,]*)?")
RE_URL_BEGINNING = re.compile(r'https?://\S+$')
RE_FULL_LINE = re.compile(r'^\S{74}$')


# These are like email.utils.parseaddr and email.utils.formataddr, but do the Unicode encoding/decoding

def parseaddr_unicode_list(l):
    result = []
    
    for name, addr in email.utils.getaddresses(l):
        name = decode_header_unicode(name)
        addr = addr.strip().lower()
        result.append((name, addr))
    return result


def formataddr_unicode(tup):
    name, addr = tup
    name = encode_header_unicode(name)
    return email.utils.formataddr((name, addr))



# ================================================
# Name functions

def initial_caps(name):
    """Convert a name to Initial Capitals."""
    
    def title_case(name):
        name = name.title()
        for prefix in ["Mc", "Mac", "O'"]:
            if name.startswith(prefix):
                name = prefix + name[len(prefix):].title()
                return name
        return name
        
    return '-'.join([title_case(part) for part in name.split('-')])
                

#def get_first_last_name_from_email(addr, use_namelist=True):
#    addr_parts = addr.split('@')[0].split('.')
#    if len(addr_parts) >= 2:
#        return initial_caps(addr_parts[0]), initial_caps(addr_parts[-1])
#    else:
#        if use_namelist:
#            # For parsing email addresses that we get in crawls, direct mails etc.
#            namelist = get_wordlist('names_male') + get_wordlist('names_female')
#            if addr_parts[0].title() in namelist:
#                return initial_caps(addr_parts[0]), ''
#            else:
#                return '', ''
#        else:
#            # For parsing assistant email addresses when someone signs up over e-mail
#            return initial_caps(addr_parts[0]), ''
        
    
def normalize_name(name):       # addr=None
        
    if not name:
        return ''
    
    if RE_EMAIL.match(name):                    # sometimes an email address is given for a name
        return ''
    
    name = re.sub('^(\\s|\'|\")*', '', name)    # remove leading spaces and quotes
    name = re.sub('(\\s|\'|\")*$', '', name)    # remove trailing spaces and quotes
    name = re.sub(r'\(.*\)', '', name)          # remove designators like (Sales)
    name = re.sub(r'\{.*\}', '', name)          # ditto
        
    #wordlist = get_wordlist('words_2of4')
    #namelist = get_wordlist('names_male') + get_wordlist('names_female')
 
    name = ' '.join(name.split())
    
    if name.count(',') == 1:
        name_parts = name.split(',')
        return '%s %s' % (initial_caps(name_parts[1].strip()), initial_caps(name_parts[0].strip()))
   
    return name
    

# ================================================
# Subject functions

def strip_subject_outer(subject):
    subject = subject.strip()
    while subject and subject[0] == '[' and subject[-1] == ']':
        subject = subject[1:-1]
    return subject

def is_subject_reply(subject):
    """Return true if subject starts with Re:
    (Note: not Fwd!)"""
    subject = strip_subject_outer(subject)
    if re.match(r"(?i)\s*[a-z][a-z](\[\w+\])?:", subject):
        return True
    return False

def is_subject_forward(subject):
    subject = strip_subject_outer(subject)
    if re.match(r"(?i)\s*(fwd?(\[\w+\])?):", subject):
        return True
    return False    
    
def is_subject_reply_or_forward(subject):
    return is_subject_reply(subject) or is_subject_forward(subject)
    
def is_subject_event_response(subject):
    subject = strip_subject_outer(subject)
    if re.match(r"(?i)\s*((accept|decline)\w*(\[\w+\])?):", subject):
        return True
    return False    
    
def strip_subject_reply_or_forward(subject):
    subject = strip_subject_outer(subject)
    while is_subject_reply_or_forward(subject):
        subject = ':'.join(subject.split(':')[1:])
        subject = strip_subject_outer(subject)
    return subject

def normalize_subject(subject):
    return ' '.join(subject.split()) 


def parse_message_ids(s):
    """Parse a string like "<hello@blah.com><bbbb@gggg.com>" and return a list of the individual "<whatever>" items.
    Useful for parsing In-Reply-To: and References: fields"""
    return re.findall(r"<[^<>]+>", s)


# ================================================
# Message part functions

# Modified from Python's email.iterators.walk
def walk_message_parts(msg, is_related=False):
    """Walk over the message tree, returning a sequence of tuples:
        (message_part, is_related_flag)
    is_related_flag is true if the message part is a "multipart/related" node or
        is any descendant of a multipart/related node. This lets us detect
        images which are meant to accompany HTML mail, as opposed to attachments,
        so we can ignore them.
    
    The walk is performed in depth-first order.  This method is a
    generator.
    """
            
    if msg.get_content_type() == 'multipart/related':
        is_related = True
    yield (msg, is_related)
    if msg.is_multipart():
        for subpart in msg.get_payload():
            for subsubpart in walk_message_parts(subpart, is_related=is_related):
                yield subsubpart
                
def get_parts_from_message(message, mimetypes):
    """Extract all the sections from a message of the given mimetype
    that aren't attachments
    Quoted-printable, and charsets like Windows-1252 are decoded
    TODO: HTML mail"""
        
    if type(mimetypes) is not list:
        mimetypes = [mimetypes]
        
    parts = []
    for part, related in walk_message_parts(message):
        #if is_part_downloadable_file(part, related, message):
        #    continue
        part_type = part.get_content_type()
        if part_type in mimetypes:
            s = part.get_payload(decode=True)            
            charset = part.get_content_charset()
            if charset:
                try:
                    s = s.decode(charset)
                except:
                    s = ''.join([char for char in s if char in string.printable])    # Hack, but works for now
            else:
                s = ''.join([char for char in s if char in string.printable])
            parts.append(s)
    
    return parts


def get_content_collapse_urls(content, line_length=74):
    "Some emails (Outlook) have long URLs split across lines; rejoin them."
    # TODO: Is the line length always 74?
    lines = content.split('\n')
    out_lines = []
    
    while lines:
        line = lines.pop(0)
        if len(line)==line_length and RE_URL_BEGINNING.match(line):
            while lines:
                line_fragment = lines.pop(0)
                if not line_fragment or line_fragment[0].isspace():
                    break
                line += line_fragment
                if not RE_FULL_LINE.match(line_fragment):
                    break
        out_lines.append(line)
        
    content = '\n'.join(out_lines)
    return content


def get_content_collapse_doublelines(content):
    "Some emails (Windows Mobile?) are double-spaced; make them single-spaced"
    count_double = len(re.findall('\n\n', content))
    count_single = len(re.findall('[^\n]\n[^\n]', content))
    if count_single == 0 or count_double / float(count_single) >= 10:
        content = content.replace('\n\n', '\n')
    return content

def get_content_collapse_triplelines(content):
    """
    Some emails have lots of extra blank lines (especially the output from html2plaintext),
    this looks bad and wreaks havoc with the footer extraction code.
    Collapse > 2 newlines in one place into 2 newlines.
    """
    lines = content.split('\n')
    lines = ['\n' if l.isspace() else l for l in lines]
    content = '\n'.join(lines)
    content = re.sub(r'\n\n\n*', '\n\n', content)
    return content


def get_all_text_from_message(py_message, collapse_double=True, collapse_triple=True, collapse_urls=True):
    """Extract all the text sections from a message, concatenate them into
    one (Unicode) string
    Quoted-printable, and charsets like Windows-1252 are decoded
    TODO: HTML mail"""

    content = '\n\n'.join([p.strip() for p in get_parts_from_message(py_message, 'text/plain')])
    
    # We don't deal with text/html, but sometimes Outlook sends text/html only that's been converted from text/plain
    if not content:
        try:
            content = '\n\n'.join([html2plaintext.html2plaintext(p, for_parsing=True) for p in get_parts_from_message(py_message, 'text/html')])
        except:
            print "ERROR on message_id", py_message['Message-Id']
            print "Headers:", dict(py_message)
            print traceback.format_exc()
    
    content = content.replace('\r\n', '\n')                 # Convert from Windows line endings
    
    if collapse_urls and 'Outlook' in py_message.get('X-Mailer', ''):
        content = get_content_collapse_urls(content)
        
    if collapse_double:
        content = get_content_collapse_doublelines(content)
        
    if collapse_triple:
        content = get_content_collapse_triplelines(content)   
        
    return strip_nonprintable(content.strip())


# ================================================
# Footer and quoted-mail functions



def get_content_sections(content, **kwargs):
    content, quoted_headers, quoted_lines, other_lines = remove_quotes(content)
    if kwargs.get('remove_footers', False):
        content, footer, footer_fields = remove_footers(content, first_name=kwargs.get('first_name'), last_name=kwargs.get('last_name'))
    else:
        content, footer, footer_fields = content, '', {}        
    return content, quoted_headers, quoted_lines, other_lines, footer, footer_fields
    
    
def remove_quotes(content):
    """
    From the text section of a message, remove anything that looks like a quote from another email
    Extract the quotes themselves, they might be useful later
    """
        
    lines = content.split('\n')
    main_lines = []
    quote_headers = []
    quote_lines = []
    other_lines = []                                            # For stuff like the Google Groups footers
        
    mode = 'main'
    i = 0
    while i < len(lines):
        l = lines[i]
        if re.match('^\s*>', l):                                # Old-fashioned quote lines
            quote_lines.append(l.split('>', 1)[1])              # Remove the leading >
            mode = 'quote-bracket'
        elif re.match('(?i).*(wrote:|sent:)\s*\**', l):         # (* at end is from html2plaintext's version of bold text)
            quote_headers.append(l)
            mode = 'quote'
        elif re.match(r'\s*(?i)On.*(,|\/).*at.*:', l) and (i < len(lines)-1) and re.match('(?i).*(wrote:|sent:)', lines[i+1]):
            quote_headers.append(l)
            quote_headers.append(lines[i+1])
            mode = 'quote'
            i += 1
        elif re.match(r'\s*--+\s*\w+ \w+\s*--+', l):                # "--- Forwarded message ---", "-----Mensaje original-----" etc.
            quote_headers.append(l)
            mode = 'headers'
        elif re.match(r'\s*_{5}_*\s*', l):                      # Separator
            if mode == 'quote':
                other_lines.append(l)                           # end of a quote
                mode = 'other'
            else:
                quote_headers.append(l)
        elif re.match('^\s*\*?(Delivered-To|Received|Return-Path|From|To|Date|Sent|Subject):', l):   # The rest of the email is probably quotes after this point
            quote_headers.append(l)
            mode = 'headers'
        elif re.search('[-~]{18}-~[-~]{18}', l):                    # Google groups footer
            other_lines.append(l)
            mode = 'other'
        elif mode == 'main':
            main_lines.append(l)
        elif mode == 'headers':
            if not l.strip():
                mode = 'quote'
            else:
                quote_headers.append(l)
        elif mode in 'quote': 
            quote_lines.append(l)
        elif mode in 'quote-bracket':
            if not re.match('^\s*>', l):
                main_lines.append(l)
                mode = 'main'
            else:
                quote_headers.append(l)
        elif mode == 'other':
            other_lines.append(l)
            if re.search('[-~]{18}-~[-~]{18}', l):
                mode = 'main'
        i += 1
            
    main_content = '\n'.join(main_lines)
    return main_content, '\n'.join(quote_headers), '\n'.join(quote_lines), '\n'.join(other_lines)


FEATURE_STRENGTH_LOW = 1
FEATURE_STRENGTH_MED = 2
FEATURE_STRENGTH_NEG_MED = -2
FEATURE_STRENGTH_HI  = 3

BEGIN_FOOTER_NONE = 0
BEGIN_FOOTER_PROBABLY = 1
BEGIN_FOOTER_HI = 2
BEGIN_FOOTER_MAX = 3

NO_MULTISTRENGTH = 'no_multistrength'

MAX_FOOTER_LINE_LENGTH = 80
NUM_FOOTER_LINES_SEARCH = 50

RE_SEPARATOR = re.compile(r"^[\s]*((---*)|(===*)|(\*\*\**))[\s]*$")
RE_GOOGLE_CALENDAR_FOOTER = re.compile(r"You are receiving this email at the account")
#RE_EMAIL = re.compile(r"\b[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]{2,6}\b")
RE_EMAIL = re.compile(r"\b[A-Za-z0-9._%+-]+@([A-Za-z0-9-]+\.)*([A-Za-z-]{2,})\b")
RE_TWITTER = re.compile(r"@?[A-Za-z0-9_]+$")
RE_URL = re.compile(r"(?i)(http://)?([A-Za-z0-9\-]+\.)+([A-Za-z]{2,6})([/?][A-Za-z0-9\-/?%=&.,]*)?")
# US phone number
RE_PHONE = re.compile(r"(?i)\b(\(?(c(ell)?|m(obile)?|h(ome)?|w(ork)?|business|f(ax)?)\)?:? *)?\+?(1[-.]?)?\(?(?P<area>\d\d\d)[)-.]? ?(?P<prefix>\d\d\d)( ?[-.] ?| )(?P<number>\d\d\d\d)(\s*(x|ext\.?|extension)(?P<extension>\d+))?\b")
# Int'l phone number, + is required
RE_PHONE2 = re.compile(r"\(?\+(?P<country_code>\d{1,3})\)?\s{0,2}(?P<number>[()\d -.]{6,}\d)") 
RE_DEVICE = re.compile(r"(?i)\s*sent (from|via) (my )?(?P<device>.*)$")    
RE_COMPANY1 = re.compile(r"([\w+\-]+\s+)*([\w+\-]+\s*),?\s*((C|c)orp\.?|Corporation|(I|i)nc\.?|Incorporated|(Ll)td\.?|Limited|(LLC|llc))\b")
institution_re = r"\b(University|Univ\.|School|College|Laboratory|Labs|Office|Laboratory|Laboratories|Institutes?|Foundation|Chamber|Group|Communications|Enterprises|Ventures|" + \
    r"Services|Sciences?|Software|Networks?)\b"
RE_COMPANY2 = re.compile(r"([\w+\-]+\s+)+" + institution_re + r"(\s+[\w+\-]+)*")    # 'Princeton University', etc.
RE_COMPANY3 = re.compile(r"([\w+\-]+\s+)*" + institution_re + r"(\s+[\w+\-]+)+")    # 'University of Iowa', etc.
title_re = r"\b(Dept\.|Department|Division|Professor|Accountant|CPA|Director|(Vice[ -])?President|Principal|Principle|" + \
    r"Executive|Manager|Chair|Chairperson|Chairman|Associate|Editor|CTO|CEO|CFO|CXO|COO|VP|Founder|Finance|Sales|Engineering|Development)"
RE_TITLE1 = re.compile(r"([A-Za-z+\-]+\s+)?([&A-Za-z+\-]+\s+)?" + title_re + r"\s*,?(\s+[&A-Za-z+\-]+)+")   # President, Blah Division
RE_TITLE2 = re.compile(r"([A-Za-z+\-]+\s+)?([&A-Za-z+\-]+\s+)?" + title_re)
RE_ADDRESS = re.compile(r"[^|,;]* (P\.?O\.? ?(B|b)ox|Suite|Ste\.|Unit|Road|(R|r)d\.|Street|(S|s)t\.|Avenue|(A|a)ve\.|Boulevard|(B|b)lvd\.?|Circle|(C|c)ir\.|Cr\.|Lane|Ln\.|Highway|Hwy\.?|Parkway|Pkwy\.?)")
RE_ZIP = re.compile(r"\b\d{5}(-\d{4})?\b")
RE_GREETING = re.compile(r"(?i)(hello|hey|greeting|\bhi\b|welcome|howdy|Dear\b|to whom)")
RE_CLOSING_LOW = re.compile(r"^\w+(\s\w+)?,\s*$")
RE_CLOSING_HI = re.compile(r"(?i)^(thanks?|thank\s*you|sincerely|all the best|best\b|cordially)([,.!]\s*)$")
RE_NAME1 = re.compile(r"([-=]|\s)*([A-Z][a-z]+)\s+([A-Z]([a-z]+|\.)\s+)?([A-Z]['\-a-z]+)\s*$")
RE_NAME2 = re.compile(r"[A-Z][a-z]+\s\s?[A-Z][\.]?\s\s?[A-Z][a-z]+")
RE_DISCLAIMER = re.compile(r"(?i)(notice|disclaimer|confidential|proprietary)")

class LineFeatures(object):
    def __init__(self, line, first_name=None, last_name=None):
        self.features = []
        self.strength = 0
        self.heat = 0
        self.peak = False
        self.line = line
        self.mangled_line = line
        self.begin_footer_strength = BEGIN_FOOTER_NONE
        self.can_begin_footer = True
        self.find_features(first_name=first_name, last_name=last_name)
        
    def __str__(self):
        return str(self.features) + ',' + str(self.strength) 
    
    def append(self, type, text, strength, scoring=None, can_begin_footer=True):
        if scoring == NO_MULTISTRENGTH:
            if not any([f[0]==type for f in self.features]):
                self.strength += strength
                self.can_begin_footer=can_begin_footer
        else:
            self.strength += strength
        self.features.append((type, text))

    def find_features(self, first_name=None, last_name=None):
                
        try:
            # --------- Line-wide features:
            
            line = self.line
            
            # quoted content is not part of a footer
            m = re.search(r"^>", line)
            if m:
                self.features, self.strength = [], -0.5
                return
            
            if len(line) > MAX_FOOTER_LINE_LENGTH:         # Doubtful that any footer line would be this long
                self.features, self.strength = [], -2.3
                return
            
            m = RE_GOOGLE_CALENDAR_FOOTER.search(line)
            if m:
                self.features, self.strength, self.begin_footer_strength = [('separator', m.group(0))], FEATURE_STRENGTH_HI, BEGIN_FOOTER_HI
                return            
            
            m = RE_SEPARATOR.search(line)
            if m:
                self.features, self.strength, self.begin_footer_strength = [('separator', m.group(0))], FEATURE_STRENGTH_HI, BEGIN_FOOTER_HI
                return
            
            m = re.search(r"(?i)with tiny keys .*", line)      # "sent via BB with tiny keys"
            if m:
                self.features, self.strength, self.begin_footer_strength = [('tiny_keys', m.group(0))], FEATURE_STRENGTH_HI, BEGIN_FOOTER_HI
                return
            
            m = RE_CLOSING_HI.search(line)
            if m:
                self.features, self.strength, self.begin_footer_strength = [('closing', m.group(0))], FEATURE_STRENGTH_HI, BEGIN_FOOTER_MAX
                return
            
#            m = RE_DISCLAIMER.search(line)
#            if m:
#                strength = FEATURE_STRENGTH_MED
#                self.append('disclaimer', line, strength)
                
            # --------- Individual features
            
            def repl_email(m):
                addr = m.group(0)
                strength = FEATURE_STRENGTH_LOW            
#                if contact and (addr.lower() == contact.email.lower()):
#                    strength = FEATURE_STRENGTH_HI
#                else:
#                    strength = FEATURE_STRENGTH_LOW
                self.append('email', addr, strength, scoring=NO_MULTISTRENGTH)
                return ''        
            line = RE_EMAIL.sub(repl_email, line)
            
            # TODO: What if too many of these in a line?
            
            def repl_url(m):
                url = m.group(0)
                strength = FEATURE_STRENGTH_LOW
                self.append('url', url, strength, scoring=NO_MULTISTRENGTH, can_begin_footer=False)
                return ''        
            line = RE_URL.sub(repl_url, line)        
            
            def repl_zip(m):
                zip = m.group(0)            
                strength = FEATURE_STRENGTH_MED
                self.append('zip_code', zip, strength, scoring=NO_MULTISTRENGTH)
                return ''        
            line = RE_ZIP.sub(repl_zip, line)  
                        
            def repl_phone(m):
                phone = m.group('area') + '-' + m.group('prefix') + '-' + m.group('number')
                try:
                    if m.group('extension'):
                        phone = phone + ' x' + m.group('extension')
                except:
                    pass
                strength = FEATURE_STRENGTH_MED
                self.append('phone', phone, strength)
                return ''        
            line = RE_PHONE.sub(repl_phone, line)
            
            # International phone numbers
            def repl_phone2(m):
                phone = '(+%s) %s' % (m.group('country_code'), m.group('number'))
                try:
                    if m.group('extension'):
                        phone = phone + ' x' + m.group('extension')
                except:
                    pass
                strength = FEATURE_STRENGTH_MED
                self.append('phone', phone, strength)
                return ''        
            line = RE_PHONE2.sub(repl_phone2, line)                    
    
            def repl_device(m):
                device = m.group('device')
                strength = FEATURE_STRENGTH_HI
                self.append('device', device, strength)        
                return ''    
            line = RE_DEVICE.sub(repl_device, line)        
      
            def repl_company(m):
                company = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('company', company, strength)
                return ''
            line = RE_COMPANY1.sub(repl_company, line)   
            line = RE_COMPANY2.sub(repl_company, line)  
            line = RE_COMPANY3.sub(repl_company, line)  

            def repl_title(m):
                title = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('title', title, strength)
                return ''
            line = RE_TITLE1.sub(repl_title, line) 
            line = RE_TITLE2.sub(repl_title, line)           
          
            def repl_address(m):
                address = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('address', address, strength)
                return ''
            line = RE_ADDRESS.sub(repl_address, line)              
            
            def repl_greeting(m):
                greeting = m.group(0)
                strength = FEATURE_STRENGTH_NEG_MED
                self.append('greeting', greeting, strength)
                return ''
            line = RE_GREETING.sub(repl_greeting, line) 
                          
            def repl_closing_low(m):
                closing = m.group(0)
                strength = FEATURE_STRENGTH_LOW
                self.append('closing', closing, strength)
                return ''
            line = RE_CLOSING_LOW.sub(repl_closing_low, line)                   

            def repl_contact_name(m):
                n = m.group(0)
                strength = FEATURE_STRENGTH_MED
                #self.begin_footer_strength = BEGIN_FOOTER_PROBABLY
                self.begin_footer_strength = BEGIN_FOOTER_MAX               # full name = beginning of footer! [eg 1/24]
                self.append('name', n, strength, scoring=NO_MULTISTRENGTH)
                return ''       
            if first_name and last_name:          
                line = re.sub(r"(?i)([-=]|\s)*(\w+\.?)?%s\s+%s" % (re.escape(first_name), re.escape(last_name)), repl_contact_name, line)
            if first_name:
                line = re.sub(r"(?i)([-=]|\s)*" + re.escape(first_name), repl_contact_name, line)
                 
            def repl_first_name(m):
                n = m.group(0)
                strength = FEATURE_STRENGTH_LOW
                self.append('name', n, strength, scoring=NO_MULTISTRENGTH)
                return ''    
            if first_name:
                line = re.sub(r"(?i)" + re.escape(first_name), repl_first_name, line)
                
            def repl_name(m):
                n = m.group(0)
                strength = FEATURE_STRENGTH_MED
                self.append('name', n, strength, scoring=NO_MULTISTRENGTH)
                return ''
            line = RE_NAME1.sub(repl_name, line)
            line = RE_NAME2.sub(repl_name, line)
    
            # line = re.sub(r"\{\w+\}", "", line)
            crap = ''.join([c for c in line if c.isalpha()])        # what remains, that we couldn't parse
            self.strength -= 2.3 * len(crap) / MAX_FOOTER_LINE_LENGTH
#            if len(self.line) > 0:
#                self.strength *= 1 - (len(line)/len(self.line))
                            
        except:
            log("find_footer_features error:", type(line), traceback.format_exc())
            
        self.mangled_line = line


def calc_footer_heatmap(lf_list):
    length = len(lf_list)
    for i in xrange(length):
        lf_list[i].heat = 0
        for j in xrange(length):
            weighting = 1 if i >= j else 0.25
            lf_list[i].heat += ((lf_list[j].strength)*(0.6**abs(i-j))) * weighting
    return lf_list

    
def find_footer_range(lf_list):

#    log("Scores:")
#    for lf in lf_list:
#        print lf.strength,
#    print
#    log("Heat:")
#    for lf in lf_list:
#        print lf.heat,
#    print
        
    max_index, max_heat = None, 0
    start, end = None, None
    
    for i, lf in enumerate(lf_list):
        if lf.begin_footer_strength == BEGIN_FOOTER_MAX:         # Definitely the beginning of a footer -- we're done
            return (i, len(lf_list))
        if lf.heat > max_heat:
            max_index, max_heat = i, lf.heat    
    
    for i, lf in enumerate(lf_list):
        if lf.begin_footer_strength == BEGIN_FOOTER_HI:         # Definitely the beginning of a footer -- we're done
            return (i, len(lf_list))
        if lf.heat > max_heat:
            max_index, max_heat = i, lf.heat
        
    # Now try to find the actual footer starting from the end of the email
    total_strength = 0
        
    for i in reversed(range(len(lf_list))):        
        lf = lf_list[i]

        #log ("begin_footer_strength:", lf.begin_footer_strength)
        
        if lf.begin_footer_strength == BEGIN_FOOTER_PROBABLY:
            return (i, len(lf_list))
            
        
        if lf.heat >= max(1, 0.5*max_heat):
            # Here's a possible starting point.
            
            start, n_blank, n_crap = i - 1, 0, 0
            while start >= 0 and n_crap <= 2: # and n_blank <= 4
                
                lf_start = lf_list[start]
                if lf_start.begin_footer_strength == BEGIN_FOOTER_PROBABLY:
                    start -=1
                    break
                
                if lf_start.line.strip() == '':
                    n_blank += 1
                elif len(lf_start.features) == 0:
                    n_crap += len(''.join(c for c in lf_start.line if c.isalnum()))/80
                                  
                if lf_list[start].heat < 0:
                    break
                start -= 1
            start += 1
            
            if lf_list[start].begin_footer_strength == 0:
                if start > 0 and lf_list[start-1].line.strip() != '':
                    while lf_list[start].line.strip() != '' and start < i and not lf_list[start].can_begin_footer:
                        start += 1
            
            end = len(lf_list)
            break
                    
        total_strength += lf.strength
        if total_strength < -20:                # Too much junk at the end -- not likely that it's a footer
            break
 
    while (start is not None) and (start < len(lf_list)) and not lf_list[start].can_begin_footer:
        start += 1
        
    if (start is not None) and start >= len(lf_list):
        start, end = None, None
        
    return (start, end)


def remove_footers(content, first_name=None, last_name=None, allow_blank_body=True):
    
    log("remove_footers start...")
    lines = content.split('\n')
    search_line_offset = max(len(lines)-NUM_FOOTER_LINES_SEARCH, 0)
    search_lines = lines[search_line_offset:]
    
    lf_list = [LineFeatures(l, first_name=first_name, last_name=last_name) for l in search_lines]
    
    footer_index_start, footer_index_end = None, None
    
#    for i, lf in enumerate(lf_list):
#        if lf.strength == BEGIN_FOOTER:
#            footer_index_start, footer_index_end = i, len(lf_list)
            
    if footer_index_start is None:        
        calc_footer_heatmap(lf_list)
    
#        for lf in lf_list:
#            try:
#                print ">>", "%d,  %3.2f  ->  %3.2f   %s" % (lf.begin_footer_strength, lf.strength, lf.heat, lf.line.encode('utf-8'))
#                print "            ", lf.features
#            except:
#                pass
            
        
        footer_index_start, footer_index_end = find_footer_range(lf_list)
        
    if footer_index_start is not None:
        footer_features = {}
        filtered_content = lines[:search_line_offset+footer_index_start]
        footer = lines[search_line_offset+footer_index_start:search_line_offset+footer_index_end]
        for lf in lf_list[footer_index_start:footer_index_end]:
            for key, value in lf.features:
                if key not in footer_features:
                    footer_features[key] = value
        filtered_content, footer, footer_features = '\n'.join(filtered_content).strip(), '\n'.join(footer).strip(), footer_features
        
        if filtered_content or allow_blank_body:
#            if Alias is not None:
#                log("*************")
#                log(filtered_content)
#                log("&&&&&&&&&&&&&&&")
            return filtered_content, footer, footer_features
        else:
            # The footer can't be the entire email... in this case, say there's no footer
            return content, '', {}
    else:
        # No footer
        return content, '', {}


def message_dict_from_raw(raw_message, folder=None, path=None, dt=None, flags=None, file_placeholders=None):
    """Helper function used by all the tasks that process emails.
    Takes an RFC822 mail message, parses its structure and returns a JSON-friendly message_dict
    (except for datetime objects, which are left as is - these must be formatted as strings
    before being sent as JSON objects)"""
        
    n = now()
    parser = email.Parser.Parser()
    py_message = parser.parsestr(strip_nonascii(raw_message))        
    
    if 'Date' in py_message:
        try:
            timetuple = parsedate_tz(py_message['Date'])
            if timetuple:
                utctimestamp = mktime_tz(timetuple)
                date_sent = datetime.fromtimestamp(utctimestamp, pytz.UTC)
                if date_sent > n:
                    date_sent = n
                # timezone = timezonestr_from_utcoffset(timetuple[-1])
            else:
                log("Couldn't parse date ", py_message['Date'])
                date_sent = n
                # timezone = timezonestr_from_utcoffset(None)                     
        except:
            log("Couldn't parse date ", py_message['Date'])
            date_sent = n
            # timezone = timezonestr_from_utcoffset(None)                
    else:
        date_sent = n
        #timezone = timezonestr_from_utcoffset(None)
    
    is_user = folder and folder['sent']
    
    fromm = None
    for name, addr in parseaddr_unicode_list(py_message.get_all('From') or []):
        from_normalized_name = normalize_name(name)
        fromm = {'name': name, 'name_normalized': from_normalized_name, 'addr': addr, 'type': 'from', 'is_user': is_user}
            
    to_all = []
    for name, addr in parseaddr_unicode_list(py_message.get_all('To') or []):
        to_all.append({'name': name, 'name_normalized': normalize_name(name), 'addr': addr, 'type': 'to'})
    for name, addr in parseaddr_unicode_list(py_message.get_all('Cc') or []):
        to_all.append({'name': name, 'name_normalized': normalize_name(name), 'addr': addr, 'type': 'cc'})
    
    if fromm:
        from_first_name, from_last_name = get_first_last_name(fromm['name_normalized'], addr=fromm['addr'])
    else:
        from_first_name, from_last_name = None, None
        
    all_text = get_all_text_from_message(py_message)   # all (plain) text concatenated together, including footers and quotes
#        body = get_content_sections(all_text, remove_footers=False)[0]     # Text with footers but no quotes. This gets stored in the DB
#        content = get_content_sections(all_text, remove_footers=True)[0]   # Text minus footers and quotes -- this doesn't get stored in the DB
    
    body = all_text[:BODY_LENGTH_MAX]                                       # Text with footers and quotes
    content = get_content_sections(all_text, remove_footers=True, first_name=from_first_name, last_name=from_last_name)[0]  # Text minus footers and quotes - for generating the summary only
    
    subject = decode_header_unicode(py_message.get('Subject', ''))
    subject_normalized = normalize_subject(subject)
    
    subject_summary, subject_more = ellipsize_and_more(strip_subject_reply_or_forward(subject_normalized), length_min=60, length_max=80)
    body_summary, body_more = ellipsize_and_more(content, length_min=150, length_max=200)
    
    summary = {
        'subject':  subject_summary,
        'body':     body_summary,
        'more':     subject_more or body_more
    }       
    
    message_dict = {
        'version':              1,
        'type':                 'rfc822',
        #'data':                 raw_message,                    # Enable this for debugging - normally we don't include the whole raw_message
        'extra': 
            {
                'folder':   folder,
                'path':     path,
                'dt':       dt,
                'flags':    flags,
                #'raw':      raw_message if settings.STORE_RAW_MESSAGES else None,
            },
        'subject':              subject,
        'subject_normalized':   subject_normalized,
        'date_sent':            date_sent,
        'message_id':           py_message.get('Message-Id', ''),
        'in_reply_to':          py_message.get('In-Reply-To', ''),
        'references':           py_message.get('References', ''),
        'from':                 fromm,
        'to_all':               to_all,
        'headers':              dict(py_message),
        'body':                 body,                               # includes footers; gets stored in DB,
        'len':                  len(body),
        'summary':              summary,                            # does NOT include footers
        'file_placeholders':    file_placeholders or []
    }
    
    for header_name in ['Sender', 'Reply-To', 'Thread-Topic', 'X-Mailer']:
        if header_name in py_message:
            message_dict['extra'][header_name] = py_message[header_name]
    
    return message_dict

