"Utilities for dealing with parts of messages (such as file attachments)"

# Parse structures returned from IMAP like this:

# '200 (UID 200 FLAGS () BODYSTRUCTURE ((("TEXT" "PLAIN" ("CHARSET" "ISO-8859-1") NIL NIL "7BIT" 32 3 NIL NIL NIL)
# ("TEXT" "HTML" ("CHARSET" "ISO-8859-1") NIL NIL "7BIT" 60 1 NIL NIL NIL) "ALTERNATIVE" ("BOUNDARY" "001517478ce80c63dd0471796a46") NIL NIL)
# ("IMAGE" "JPEG" ("NAME" "03cute.large1.jpg") NIL NIL "BASE64" 99088 NIL ("ATTACHMENT" ("FILENAME" "03cute.large1.jpg")) NIL) "MIXED" 
# ("BOUNDARY" "001517478ce80c63e40471796a48") NIL NIL))'

import re, cStringIO
from utils import *

# thanks to http://effbot.org/zone/simple-iterator-parser.htm

IMAP_TOKEN_RE = re.compile(r'\s*(?:(\(|\))|(\w+)|("(?:(?:\\")|[^"])*"))')

TOKEN_TYPE_PUNC = 'punc'
TOKEN_TYPE_WORD = 'word'
TOKEN_TYPE_STRING = 'string'
    
class MIMEPartDescriptor(object):
    def __init__(self):
        self.mimepath = None
        self.name = None
        self.mimetype = None
        self.mimesubtype = None
        self.content_id = None
        
    def as_dict(self):
        return {
            'mimepath':     self.mimepath,
            'name':         self.name,
            'mimetype':     self.mimetype,
            'mimesubtype':  self.mimesubtype,
            'content_id':   self.content_id
        }
                    

class IMAPMIMEPartDescriptor(MIMEPartDescriptor):
    def __init__(self, mimepath, struct):
        self.mimepath = mimepath
        if len(struct) > 6:
            self.size = int(struct[6])
        else:
            self.size = None
        if len(struct) > 2 and type(struct[2]) is list and struct[2][0].lower() == 'name':
            self.name = struct[2][1]
        else:
            self.name = None
        self.mimetype = struct[0].lower()
        self.mimesubtype = struct[1].lower()
        self.content_id = struct[3] if struct[3] != 'NIL' else None
        
        
class OWAMIMEPartDescriptor(MIMEPartDescriptor):
    def __init__(self, mimepath, name, mimetype, mimesubtype):
        self.mimepath = mimepath
        self.name = name
        self.mimetype = mimetype
        self.mimesubtype = mimesubtype
        self.content_id = None
        
        
class IMAPBodyStructure():
    
    def __init__(self, data):
        s = '(' + self.glue(data) + ')'
        tokens = self.tokenize(s)
        self.struct = self.atom(tokens.next, tokens.next())
        
    def atom(self, next, token):
        if token[0] is TOKEN_TYPE_PUNC:
            if token[1] == "(":
                out = []
                token = next()
                while token[1] != ")":
                    out.append(self.atom(next, token))
                    token = next()
            return out
        elif token[0] is TOKEN_TYPE_STRING:
            return token[1]
        elif token[0] is TOKEN_TYPE_WORD:
            return token[1]
        else:
            raise Exception()
    
    def unquote(self, s):
        s = s.replace('\\\\', '\\').replace('\\"', '"')      #.replace("\\'", "'")
        return decode_header_unicode(s)
    
    def tokenize(self, s):        
        s = list(s)
        
        while s:
            c = s.pop(0)
            
            if c in ['(', ')']:
                yield (TOKEN_TYPE_PUNC, c)
                
            elif c.isalnum():
                result = ''
                while c.isalnum():
                    result += c
                    c = s.pop(0)
                s[0:0] = c
                yield (TOKEN_TYPE_WORD, result)
                
            elif c == '"':
                result = ''
                c = s.pop(0)
                while c != '"':
                    result += c
                    if c == '\\':
                        c = s.pop(0)
                        result += c
                    c = s.pop(0)
                yield (TOKEN_TYPE_STRING, self.unquote(result))
                
            elif c == '{':
                digits = ''
                c = s.pop(0)
                while c.isdigit():
                    digits += c
                    c = s.pop(0)
                result = ''
                for i in range(int(digits)):
                    result += s.pop(0)
                yield (TOKEN_TYPE_STRING, self.unquote(result))
            
    def get_bodystructure(self):
        for i, token in enumerate(self.struct[1]):
            if token == 'BODYSTRUCTURE':
                return self.struct[1][i+1]            
        return None
    
    def glue(self, data):
        if type(data) in [list, tuple]:
            return ''.join(self.glue(x) for x in data)
        else:
            return data
    
    def _get_partial_partlist(self, struct, prefix):
        result = []
        
        if type(struct[0]) is list:
            for i, child in enumerate(struct):
                if type(child) is list:
                    if prefix.isalpha():
                        new_prefix = str(i+1)
                    else:
                        new_prefix = '%s.%d' % (prefix, i+1)
                    result.extend(self._get_partial_partlist(child, new_prefix))
                else:
                    break       # 'mixed', 'boundary' or something else we don't care about
        else:
            result = [IMAPMIMEPartDescriptor(prefix, struct)]
                
        return result  
            
    def get_partlist(self):
        struct = self.get_bodystructure()
        return self._get_partial_partlist(struct, 'TEXT')
    
#    def find_text_part_names(self):
#        struct = self.get_bodystructure()
#        
#        part_names = ['RFC822.HEADER']  # don't use just "HEADER"; doesn't seem to work on GMail
#        
#        if struct[0] == 'TEXT':
#            part_names.append('BODY[1]')
#        else:
#            for i, part in enumerate(struct):
#                if (type(part) is tuple) and len(part) > 0:
#                    if (type(part[0]) != str) or part[0] == 'TEXT':
#                        part_names.append('BODY[%d]' % (i+1))
#        
#        return part_names           
        

# Test of getting specific parts from IMAP


#def imaptestl():
#    import imaplib, calendar
#    # server = imaplib.IMAP4_SSL('imap.gmail.com', 993)
#    # server.login("severely.666", "tgoeapex")
#    server = imaplib.IMAP4('noted.ccl')
#    server.login("egilliam", "blah")
#    status, n_messages = server.select('"%s"' % 'INBOX', True)
#    since = now() - 1*ONE_DAY
#    since_string = '"%02d-%s-%04d"' % (since.day, calendar.month_abbr[since.month], since.year)
#    status, data = server.uid('SEARCH', 'SINCE', since_string)
#    paths = data[0].split()
#    for path in paths:
#        status, data = server.uid('FETCH', path, '(FLAGS BODYSTRUCTURE)')
#        bs = IMAPBodyStructure(data)
#        print "----------\n\n"
#        pprint.pprint(bs.get_bodystructure())
##        fs = bs.get_partlist()
##        text_parts = [tup for tup in fs if tup[1]=='text' and tup[4] is None]
##        file_parts = [tup for tup in fs if tup[3] and tup[4]]
##        print path
#        #pprint.pprint(text_parts)
#        #pprint.pprint(file_parts)
#        
#   
#
#def imaptest():
#    import imaplib
#    server = imaplib.IMAP4('noted.ccl')
#    server.login("egilliam", "blah")
#    status, n_messages = server.select('"%s"' % 'INBOX', True)
#    status, data = server.uid('FETCH', '9328', '(FLAGS BODYSTRUCTURE)')
#    bs = IMAPBodyStructure(data)
#    fs = bs.get_partlist()
#    pprint.pprint(fs)
#    
#    
#    
##    part_names = bs.find_text_part_names()
##    part_string = '(' + ' '.join(part_names) + ')'
#    
#    print part_string
#    status, data = server.uid('FETCH', '200', part_string)
#    return status, data
#
#def imaptestb():
#    import imaplib
#    server = imaplib.IMAP4('noted.ccl')
#    server.login("egilliam", "blah")
#    status, n_messages = server.select('"%s"' % 'INBOX', True)
#    status, data = server.uid('FETCH', '9331', '(FLAGS BODYSTRUCTURE)')
#    bs = IMAPBodyStructure(data)
#    fs = bs.get_partlist()
#    pprint.pprint(fs)
#    
#    
#    print part_string
#    status, data = server.uid('FETCH', '200', part_string)
#    return status, data
#
#
#def imaptest2():
#    import imaplib
#    server = imaplib.IMAP4_SSL('imap.gmail.com', 993)
#    server.login("severely.666", "blah")
#    status, n_messages = server.select('"%s"' % 'INBOX', True)
#    status, data = server.uid('FETCH', '204', '(FLAGS BODYSTRUCTURE)')
#    bs = IMAPBodyStructure(data)
#    fs = bs.get_partlist()
#    pprint.pprint(fs)
#    
#    
#    print part_string
#    status, data = server.uid('FETCH', '20', part_string)
#    return status, data


# TEST = IMAPBodyStructure('200 (UID 200 FLAGS () BODYSTRUCTURE ((("TEXT" "PLAIN" ("CHARSET" "ISO-8859-1") NIL NIL "7BIT" 32 3 NIL NIL NIL)("TEXT" "HTML" ("CHARSET" "ISO-8859-1") NIL NIL "7BIT" 60 1 NIL NIL NIL) "ALTERNATIVE" ("BOUNDARY" "001517478ce80c63dd0471796a46") NIL NIL)("IMAGE" "JPEG" ("NAME" "03cute.large1.jpg") NIL NIL "BASE64" 99088 NIL ("ATTACHMENT" ("FILENAME" "03cute.large1.jpg")) NIL) "MIXED" ("BOUNDARY" "001517478ce80c63e40471796a48") NIL NIL))')
# TEST2 = IMAPBodyStructure('201 (UID 201 FLAGS () BODYSTRUCTURE ((("TEXT" "PLAIN" ("CHARSET" "ISO-8859-1") NIL NIL "7BIT" 2 1 NIL NIL NIL)("TEXT" "HTML" ("CHARSET" "ISO-8859-1") NIL NIL "7BIT" 6 1 NIL NIL NIL) "ALTERNATIVE" ("BOUNDARY" "0015174764da25f3ee04717a3dbb") NIL NIL)("APPLICATION" "OCTET-STREAM" ("NAME" "even") NIL NIL "BASE64" 195812 NIL ("ATTACHMENT" ("FILENAME" "even")) NIL) "MIXED" ("BOUNDARY" "0015174764da25f3fa04717a3dbd") NIL NIL))')

# TEST = '200 (UID 200 FLAGS () BODYSTRUCTURE ((("TEXT" "PLAIN" ("CHARSET" "ISO-8859-1") NIL NIL "7BIT" 32 3 NIL NIL NIL)("TEXT" "HTML" ("CHARSET" "ISO-8859-1") NIL NIL "7BIT" 60 1 NIL NIL NIL) "ALTERNATIVE" ("BOUNDARY" "001517478ce80c63dd0471796a46") NIL NIL)("IMAGE" "JPEG" ("NAME" "03cute.large1.jpg") NIL NIL "BASE64" 99088 NIL ("ATTACHMENT" ("FILENAME" "03cute.large1.jpg")) NIL) "MIXED" ("BOUNDARY" "001517478ce80c63e40471796a48") NIL NIL))')




    
    