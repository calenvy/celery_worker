from utils import *
import time as time_
import urlparse, urllib, urllib2, base64, httplib, hashlib, imaplib, re
from BeautifulSoup import *
from Cookie import SimpleCookie
import mimepart

class PasswordNeededException(Exception):
    pass


class MailServer(object):
    
    @staticmethod
    def new_from_type(settings):
        server_service = settings['service']
        if server_service ==  MAILSERVICE_GMAIL:
            server = IMAPGmail(settings)            
        elif server_service == MAILSERVICE_GOOGLE_APPS:
            server = IMAPGoogleApps(settings)
        elif server_service  == MAILSERVICE_OTHER_IMAP:
            server = IMAP(settings)
        elif server_service == MAILSERVICE_OTHER_OWA:
            server = OWA(settings)
#       elif server_service in [MAILSERVICE_OTHER_POP, MAILSERVICE_YAHOO, MAILSERVICE_HOTMAIL]:
#            server = POP(settings)
        else:
            raise NotImplementedError("Unknown server service %s" % server_service)
        return server
    
    def __init__(self):
        self._cookies = SimpleCookie()
        self._headers = [('User-Agent', 'Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.5; en-US; rv:1.9.0.10) Gecko/2009042315 Firefox/3.0.10)')]
   
    # Handy HTTPS functions (used by OWA, Google Mail captcha, maybe more later)
    
    def base_url(self, full_url):
        urltype, url = urllib.splittype(full_url)
        host, selector = urllib.splithost(url)
        return "%s://%s" % (urltype, host)       
        
    def connection(self, full_url, old=None):
        urltype, url = urllib.splittype(full_url)
        host, selector = urllib.splithost(url)
        if urltype.lower()=='https':
            h = httplib.HTTPSConnection(host)
        elif urltype.lower()=='http':
            h = httplib.HTTPConnection(host)
        #h.set_debuglevel(1)
        return h
        
    def raw_get_page(self, full_url, post_data=None, headers=[], max_len=None):
        """
        Helper method that gets the given URL, handling the sending and storing
        of cookies. Returns the requested page as a string.
        If max_len is not None and the length is greater than max_len, raises an exception.
        """
        
        full_url = str(full_url)
        urltype, url = urllib.splittype(full_url)
        host, selector = urllib.splithost(url)
        
        try:
            log("raw_get_page 1")
            h = self.connection(full_url)
            log("raw_get_page 2")
            if post_data is not None:
                post_data = str(post_data)      # Must be str, not Unicode
                h.putrequest('POST', selector)
                h.putheader('Content-Type', 'application/x-www-form-urlencoded')
                h.putheader('Content-Length', '%d' % len(post_data))
            else:
                h.putrequest('GET', selector)
            log("raw_get_page 3")
            for k, v in (headers + self._headers):
                h.putheader(k, v)
            log("raw_get_page 4")
            cookies = self._cookies.output(header='', sep='; ').strip()
            if cookies:
                h.putheader('Cookie', cookies)
            h.endheaders()
            if post_data is not None:
                h.send(post_data + '\r\n')
            log("raw_get_page 5")
            resp = h.getresponse()
            log("raw_get_page 6")
            errcode = resp.status
            response_headers = resp.getheaders()
            content = ''
            log("raw_get_page 7")
            while 1:
                data = resp.read()
                if data:
                    content += data
                else:
                    break
            log("raw_get_page 8")

            return errcode, response_headers, content
        except:
            log("raw_get_page: error")
            raise
        
    def redirect_get_page(self, full_url, post_data=None, headers=[], max_redirects=4, set_cookies=False):
        '''Wrapper for get_raw_page that handles redirects and cookies
        Returns (content, final_url)'''
        
        log("redirect_get_page: ", full_url)
                    
        errcode, response_headers, content = self.raw_get_page(full_url, post_data=post_data, headers=headers)
        hdict = dict(response_headers)
        
        if errcode in [200, 301, 302]:
            if set_cookies and ('set-cookie' in hdict):
                cookies = hdict['set-cookie']
                for c in cookies.split(';'):
                    for cc in c.split(','):
                        cc = cc.strip()
                        if not any([name in cc.lower() for name in ['path', 'httponly', 'secure']]):
                            log("Setting a cookie: ", cc)
                            self._cookies.load(cc)
                    
        if errcode in [301, 302]:
            # Redirect
            if 'location' in hdict:
                if max_redirects > 0:
                    redirect_url = urllib.basejoin(full_url, hdict['location'])                        
                    # Don't propagate the headers when following a redirect
                    return self.redirect_get_page(redirect_url, headers=headers, max_redirects=max_redirects-1)
                else:
                    raise Exception('Ran out of redirects')
            else:
                raise Exception('Invalid HTTP 302 response')
                
        if errcode == 200:
            return content, full_url
            
        raise Exception('Cannot load page, errcode %d' % errcode)
        
    def get_page(self, *args, **kwargs):
        content, final_url = self.redirect_get_page(*args, **kwargs)
        return content           
    
    
    def folder_priority(self, name, parent):
        """Based on a folder name, return a dictionary which indicates
        where this folder should be in the crawl order, among other attributes
        Returns None if the folder shouldn't be crawled at all"""

        # Skip these
        if (re.search(r'(?i)(spam|draft|junk|tasks|personal|contacts|virus|rss|sync|error|apple mail|to do|calendar|outbox|journal|shared)', name)):
            return None
        
        # Also skip deleted messages
        if (re.search(r'(?i)(trash|delete)', name)):
            return None
        
        inbox = 'inbox' in name.lower()
        sent = 'sent' in name.lower()
        
        if inbox:
            priority = 1
        elif sent:
            priority = 0          
        elif parent and parent != '/':      # Subfolders are lower priority (i.e., higher number)
            priority = 3
        else:
            priority = 2
            
        return {
            'name':     name,               # name of this folder
            'parent':   parent,             # name of the parent folder, if any
            'priority': priority,           # order to crawl folders - lower number gets crawled first
            'inbox':    inbox,              # boolean - whether this folder is the user's main inbox
            'sent':     sent,               # boolean - whether this folder is the user's main "sent items" folder
            'all_mail': False               # For generic IMAP there is no special "all mail" folder
        }
    
    def ping(self):
        "Call this periodically to keep a connection alive."
        return False

    def fetch_raw_message(self, path):
        raise NotImplemented    # abstract method

    def fetch_raw_message_part(self, path, mimepath):
        raise NotImplemented    # abstract method



class IMAP(MailServer):
 
    re_LIST = re.compile(r'^\s*\((?P<flags>[^)]*)\)\s+((?P<parent>\w+)|"(?P<parent_quoted>.+)")\s+((?P<name>\w+)|"(?P<name_quoted>.+)")\s*$')
 
    def __init__(self, settings):
        super(IMAP, self).__init__()         
        self.servername = settings.get('server', '')
        self.username = settings.get('username', '')
        self.use_ssl = settings.get('use_ssl', False)
        self.port = settings.get('port', None)
        self.password = settings.get('password', None)      # None means use the Celery stored password
        self.server = None
        self.folder = None
     
    def _get_server(self):
        if self.use_ssl:
            if self.port:
                return imaplib.IMAP4_SSL(self.servername, self.port)
            else:
                return imaplib.IMAP4_SSL(self.servername)
        else:
            if self.port:
                return imaplib.IMAP4(self.servername, self.port)
            else:
                return imaplib.IMAP4(self.servername)            
            
           
    def login(self):
        try:                 
#            key = self.key    
            self.server = self._get_server()
            if not self.server:
                raise Exception("Can't get server right now")
            
            try:
                self.server.login(self.username, self.password)
            except:
                self.server = None
                raise
            
        except:
            self.server = None
            raise
                    
    def logout(self):        
        log("IMAP.logout: %d" % os.getpid())
        
        if self.server:
            self.server.logout()
            self.server = None
            self.folder = None
    
    def ping(self):
        if self.server:
            self.server.noop()
            return True
        return False
    
    def get_folder_list(self):
        
        if not self.server:
            raise Exception("get_folder_list: must be logged in to do this")
            
        status, data = self.server.list()
        if status != 'OK':
            raise Exception("crawl: could not retrieve list of folders")
            
        result = []
        # IMAP returns strings like '(\\Noselect \\HasChildren) "/" "[Gmail]"'
        for folder_str in data:
            m = self.__class__.re_LIST.match(folder_str)
            if m:
                flags = m.group('flags').split()
                parent = m.group('parent') or m.group('parent_quoted')
                name = m.group('name') or m.group('name_quoted')
                if r'\Noselect' not in flags:
                    log("%%%%%%%%", name)
                    f = self.folder_priority(name, parent)
                    if f is not None:
                        f['flags'] = flags  # this is specific to IMAP
                        result.append(f)
                                
        result.sort(key=lambda f: f['priority'])
        log("get_folder_list: returning these folders:", result)
        return result
    
    def select_folder(self, folder, since=None, max_msgs=None, highest_path=None, highest_date=None, get_tuples=True, read_only=True):
        import calendar
        
        if not self.server:
            raise Exception("select_folder: must be logged in")        
    
        if type(folder) is dict:
            folder = folder['name']
                
        status, n_messages = self.server.select('"%s"' % folder, read_only)
        if status != 'OK':
            raise Exception("select_folder: could not select folder %s" % folder)
        
        self.folder = folder
        
        result = True
        if get_tuples:
            if since:          
                since = since - ONE_DAY     # allow some slack, as mailservers may be in different timezones
                since_string = '"%02d-%s-%04d"' % (since.day, calendar.month_abbr[since.month], since.year)
                status, data = self.server.uid('SEARCH', 'SINCE', since_string)
                if status != 'OK':
                    raise Exception("select_folder: could not search SINCE %s" % since_string)
                result = data[0].split()
                result = [(str(uid), None) for uid in result]
            elif highest_path:
                start_uid=int(highest_path)+1
                status, data = self.server.uid('SEARCH', 'UID', '%s:*' % str(start_uid))
                result = data[0].split()
                result = [(str(uid), None) for uid in result if int(uid) >= start_uid]   # needed because * might be lower
            else:
                result = True
        
        if max_msgs is not None:
            result = result[-max_msgs:]         # List is in ascending order by date, so get the last <max_msgs>
            
        return result
    
    
    def _strip_headers(self, headers, remove=['Content']):
        "Remove headers from a message header block starting with 'Content'"
        headers = headers.replace('\r\n', '\n')
        lines = headers.split('\n')
        out_lines = []
        while lines:
            l = lines.pop(0)
            if not l:
                continue
            if any(l.startswith(word) for word in remove):
                while lines:
                    l = lines.pop(0)
                    if l and not l[0].isspace():
                        lines[0:0] = [l]      # too far, push it back
                        break
            else:
                out_lines.append(l)
        return '\n'.join(out_lines)
    
    def _imap_parts_to_fetch(self, partlist):
        "Format a list of message part identifiers suitable for the IMAP FETCH command"
        result = []
        for p in partlist:
            if not p.isalpha():                     # For TEXT and HEADER, don't fetch the MIME part
                result.append('BODY[%s.MIME]' % p)
            result.append('BODY[%s]' % p)
        return result
    
    def _glue_message_parts(self, data, imap_parts_to_fetch):
        raw_message_parts = []
        for imap_part in imap_parts_to_fetch:
            raw_message_part = None
            for result in data:
                if imap_part in result[0]:
                    raw_message_part = self._strip_headers(result[1]) if 'HEADER' in imap_part else result[1]
                    break
            if raw_message_part is None:
                raise Exception()
            raw_message_parts.append(raw_message_part)
        raw_message = '\n'.join(raw_message_parts)
        return raw_message
            
    def _fetch_text_only_message(self, path, partlist):
        "Fetch a message containing only one MIME part, which is the first text/plain or text/html part we care about"
        
        # Iterate through the part list, find the text part we care about
        
        text_part = None
        for p in partlist:
            if p.mimetype=='text' and p.mimesubtype=='plain':
                text_part = p
                break
            
        if not text_part:
            for tup in partlist:
                if p.mimetype=='text' and p.mimesubtype=='html':
                    text_part = p
                    break    
        
        parts_to_fetch = ['HEADER']
                
        if text_part:
            parts_to_fetch.append(text_part.mimepath)
                    
        if text_part and text_part.mimepath == 'TEXT':
            return self._fetch_entire_message(path)
        else:
            imap_parts_to_fetch = self._imap_parts_to_fetch(parts_to_fetch)
            status, data = self.server.uid('FETCH', path, '(' + ' '.join(imap_parts_to_fetch) + ')')
            if status != 'OK':
                raise Exception("fetch_raw_message: could not fetch parts of message UID%s from folder %s" % (path, self.folder))        
            raw_message = self._glue_message_parts(data, imap_parts_to_fetch)
            
        return raw_message, {}
        
    def _fetch_entire_message(self, path):  
        status, data = self.server.uid('FETCH', path, '(RFC822)')
        if status != 'OK':
            raise Exception("fetch_raw_message: could not fetch parts of message UID%s from folder %s" % (path, self.folder))        
        raw_message = data[0][1]
        return raw_message, {}
        
    def _get_ics_list(self, partlist):
        result = []
        for p in partlist:
            if ('%s/%s' % (p.mimetype, p.mimesubtype)) in ICS_MIMETYPES:
                result.append(p)
        return result  
    
    def _get_file_list(self, partlist):
        result = []
        for p in partlist:
            if p.size and p.name and ('%s/%s' % (p.mimetype, p.mimesubtype)) not in ICS_MIMETYPES:
                result.append(p)
        return result  
        
    def fetch_raw_message(self, path):
        """
        External interface to the IMAP fetcher:
        Get one message from IMAP, but only fetch the text (or html) parts
        Return a hacked rfc822 raw message without any file attachments.
        
        If message contains an .ics file, just download the whole message (for now.)
        (We rarely see emails with big attachments *and* an .ics.)
        """
                 
        if not self.server:
            raise Exception("fetch_raw_message: must be logged in")
        if not self.folder:
            raise Exception("fetch_raw_message: must be logged in")        
        
        status, data = self.server.uid('FETCH', path, '(FLAGS BODYSTRUCTURE)')
        if status != 'OK':
            raise Exception("fetch_raw_message: could not fetch message UID%s from folder %s" % (path, self.folder))
        
        try:
            bs = mimepart.IMAPBodyStructure(data)
            partlist = bs.get_partlist()        # Get a flat list of the message parts (actually, just the leaves of the message tree)
                       
            filelist = self._get_file_list(partlist)
            icslist = self._get_ics_list(partlist)
        
            if icslist:                             # For now, just download entire message if there's an .ics 
                raw_message, flags = self._fetch_entire_message(path)
            else:
                raw_message, flags = self._fetch_text_only_message(path, partlist)
        except:
            status, data = self.server.uid('FETCH', path, '(RFC822)')
            if status != 'OK':
                raise Exception("fetch_raw_message: could not fetch raw message UID%s from folder %s" % (path, self.folder))        
            raw_message, flags, filelist = data[0][1], {}, []
            
        return (raw_message, flags, filelist)

    def fetch_raw_message_part(self, path, mimepath):
        """
        External interface to the IMAP fetcher:
        Get one message part from a message (this lets users click on a file link and 
        retrieve the original file from IMAP if it's not stored locally).
        """
        
        if not self.server:
            raise Exception("fetch_raw_message: must be logged in")
        if not self.folder:
            raise Exception("fetch_raw_message: must be logged in")     
        
        imap_parts_to_fetch = self._imap_parts_to_fetch([mimepath])
        status, data = self.server.uid('FETCH', path, '(' + ' '.join(imap_parts_to_fetch) + ')')
        if status != 'OK':
            raise Exception("fetch_raw_message: could not fetch parts of message UID%s from folder %s" % (path, self.folder))        
        
        raw_message_part = self._glue_message_parts(data, imap_parts_to_fetch)
        return raw_message_part, True           # True means it still needs to be decoded

    def store_raw_message(self, folder, msg):
        if type(folder) is dict:
            folder = folder['name']
            
        if not self.server:
            raise Exception("store_raw_message: must be logged in")   
        
        status, data = self.server.append(folder, '', imaplib.Time2Internaldate(time_.time()), str(msg))
        if status != 'OK':
            raise Exception("store_raw_message: could not store message in folder %s" % folder)
        
        
class IMAPGmail(IMAP):
    

    def get_captcha(self):
        try:
            log("get_captcha...")
            
            html = self.get_page(self.get_captcha_url())
            soup = BeautifulSoup(html)
            img = soup.find("img", attrs={'alt': 'Visual verification'})   
            slug = create_slug()
            log("Here's the slug:", slug)
            captcha = {\
                'slug':             slug,
                'img':              self.get_page(img['src']),
                'unlockcaptchatoken': soup.find(attrs={'name': 'unlockcaptchatoken'})['value'],
                'unlockcaptchaurl': soup.find(attrs={'name': 'unlockcaptchaurl'})['value'],
                'unlockcaptchatoken_audio': soup.find(attrs={'name': 'unlockcaptchatoken_audio'})['value'],
                'unlockcaptchaurl_audio': soup.find(attrs={'name': 'unlockcaptchaurl_audio'})['value'], 
            }   
            return captcha
        except:
            return None
        
    def submit_captcha_answer(self, captcha, answer):
        try:
            post_data = str(urllib.urlencode({\
                'Email': self.username,
                'Passwd': self.password,
                'unlockcaptchatoken': captcha['unlockcaptchatoken'],
                'unlockcaptchaurl': captcha['unlockcaptchaurl'],
                'unlockcaptchatoken_audio': captcha['unlockcaptchatoken_audio'],
                'unlockcaptchaurl_audio': captcha['unlockcaptchaurl_audio'],
                'unlockcaptchacaptcha': answer
            }))
            html = self.get_page(self.submit_captcha_url(), post_data=post_data)
            return "successfully unlocked" in html.lower()  
        except:
            return None     
        
    def get_captcha_url(self):
        return 'https://www.google.com/accounts/DisplayUnlockCaptcha'
    
    def submit_captcha_url(self):
        return 'https://www.google.com/accounts/UnlockCaptcha?'

    def folder_priority(self, name, parent):
        """Based on a folder name, return a dictionary which indicates
        where this folder should be in the crawl order, among other attributes
        Returns None if the folder shouldn't be crawled at all"""
    
        if (re.search(r'(?i)\[.*\]/all mail', name)):   # The [Gmail]/All Mail folder
            return {
                'name':     name,
                'parent':   parent,
                'priority': 0,
                'inbox':    False,
                'sent':     False,
                'all_mail': True
            }            
    
        return super(IMAPGmail, self).folder_priority(name, parent)
        

class IMAPGoogleApps(IMAPGmail):
    """IMAPGoogleApps is same as IMAPGMail, but we always present the captcha and the captcha URL is different"""
        
    def get_captcha_url(self):
        domain = self.username.split('@')[-1]
        return 'https://www.google.com/a/%s/UnlockCaptcha' % domain
        
    def submit_captcha_answer(self, captcha, answer):
        try:
            post_data = str(urllib.urlencode({\
                'userName': self.username.split('@')[0],
                'password': self.password,
                'unlockcaptchatoken': captcha['unlockcaptchatoken'],
                'unlockcaptchaurl': captcha['unlockcaptchaurl'],
                'unlockcaptchatoken_audio': captcha['unlockcaptchatoken_audio'],
                'unlockcaptchaurl_audio': captcha['unlockcaptchaurl_audio'],
                'unlockcaptchacaptcha': answer
            }))
            html = self.get_page(self.get_captcha_url(), post_data=post_data)
            return any([phrase in html.lower() for phrase in ["successfully unlocked", "unlock request submitted"]])
        except:
            return None      
    
#class POP(MailServer):
#  
#    def __init__(self, settings):
#        super(POP, self).__init__() 
#        self.servername = settings.get('server', '')
#        self.username = settings.get('username', '')
#        self.password = settings.get('password', '')
#        self.use_ssl = settings.get('use_ssl', False)
#        self.port = settings.get('port', None)
#        self.server = None
#        self.folder = None
#        self._uids = {}
#     
#    def login(self):
#        try:
#            if self.use_ssl:
#                if self.port:
#                    self.server = poplib.POP3_SSL(self.servername, self.port)
#                else:
#                    self.server = poplib.POP3_SSL(self.servername)
#            else:
#                if self.port:
#                    self.server = poplib.POP3(self.servername, self.port)
#                else:
#                    self.server = poplib.POP3(self.servername)
#            self.server.user(self.username)
#            self.server.pass_(self.password)  
#            self._uids = {}
#        except:
#            self.server = None
#            raise
#                    
#    def logout(self):
#        if self.server:
#            self.server.quit()
#            self._uids = {}            
#            self.server = None
#            self.folder = None
#    
#    def get_folder_list(self):
#        # POP doesn't have folders, just return a single folder and call it the empty string
#        
#        if not self.server:
#            raise Exception("get_folder_list: must be logged in to do this")
#                    
#        result = [{
#            'name':     '',
#            'flags':    '',
#            'parent':   '',
#            'priority': 0
#        }]
#        return result
#        
#    def select_folder(self, folder, since=None, max_msgs=None, highest_path=None, highest_date=None, get_tuples=True):
#              
#        if not self.server:
#            raise Exception("select_folder: must be logged in")  
#        
#        if type(folder) is dict:
#            folder = folder['name']
#        if folder != '':
#            raise Exception("select_folder: POP3 does not support folders")
#        
#        status, msg_list, num_bytes = self.server.uidl()
#        if not status.startswith('+OK'):
#            raise Exception("select_folder: could not retrieve list of messages") 
#        
#        # Take the list of msg_num to UID mappings, each entry looks like this:
#        #  '29 GmailId12105573e37a78bd'
#        #  UIDs are permanent, msg_nums are just for this session
#        uid_tuples = []
#        for uid_str in msg_list:
#            msg_num, uid = uid_str.split()
#            msg_num = int(msg_num)
#            self._uids[uid] = msg_num
#            uid_tuples.append((uid, msg_num))
#        
#        self.folder = folder
#            
#        # TODO: handle all those options above
#        
#        result = True
#        if get_tuples:
#            result = [(uid, None) for uid, msg_num in uid_tuples]
#    
#        if max_msgs is not None:
#            result = result[-max_msgs:]
#        
#        # Ignore the other options; no good way to handle them in POP
#        return result
#        
#    def fetch_raw_message(self, path):
#        
#        # Path here is the UID
#                
#        if self.server is None:
#            raise Exception("fetch_raw_message: must be logged in")
#        if self.folder is None:
#            raise Exception("fetch_raw_message: must be logged in")    
#        
#        if path not in self._uids:
#            raise Exception("fetch_raw_message: message UID%s not available on POP server" % path)
#        
#        msg_num = self._uids[path]
#        status, msg_lines, num_bytes = self.server.retr(msg_num)
#        if not status.startswith('+OK'):
#            raise Exception("fetch_raw_message: could not retrieve message UID%s not available on POP server") 
#        
#        raw_message = strip_nonascii('\n'.join(msg_lines))
#        flags = {}
#        return raw_message, flags, []
 

class InvalidLogin(Exception):
    pass

class OWA(MailServer):
    """Outlook Web Access screen-scraping"""
    
    def __init__(self, settings):
        super(OWA, self).__init__() 

        self.url = settings['server']
        if '://' not in self.url:
            self.url = 'https://' + self.url
        self.username = settings['username']
        self.password = settings['password']
        # self.use_ssl = settings['use_ssl']
        # self.port = settings['port']
        self.server = None
        self.folder = None
        self.version = ''                       # '2003' or '2007'
        self.tzinfo = settings.get('tzinfo', pytz.UTC)
        self._base_href = None
    
    def _get_login_page(self, url):
        try:
            html, frontpage_url = self.redirect_get_page(url, set_cookies=False)
        except:
            return False, None, None, None, None
        soup = BeautifulSoup(html) #, convertEntities=BeautifulSoup.HTML_ENTITIES)
        forms = soup.findAll('form', {'name': 'logonForm'})
        if forms:
            # Like the coa account
            form = forms[0]
            form_path = str(form['action'])       # e.g., "/exchweb/bin/auth/owaauth.dll"
            destination_input = form.findAll('input', {'name': 'destination'})
            destination_url = str(destination_input[0]['value'])
            post_data = str(urllib.urlencode({
                # I got these settings from the coa Exchange page [Emil]
                'destination': str(destination_url),
                'flags': '0',
                'username': str(self.username),
                'password': str(self.password),
                'SubmitCreds': 'Log On',
                'trusted': '0',             # "public or shared computer"
            }))        
            return True, form_path, frontpage_url, destination_url, post_data
        else:
            return False, None, None, None, None
        
                  
    def login(self):        
        form_url = None
        
        # Get the front page and extract the form URL              
        success, form_path, frontpage_url, destination_url, post_data = self._get_login_page(self.url)
        if not success:
            time_.sleep(1)
            
            url = urlparse.urljoin(self.base_url(self.url), 'exchange/')
            success, form_path, frontpage_url, destination_url, post_data = self._get_login_page(url)
            if not success:
                # This might be like the old mail2web.com account, 
                #  no form directly on the page so try a certain magic URL with BasicAuth
                self._headers.append(('Authorization', 'Basic ' + base64.b64encode(self.username + ':' + self.password)))
                form_path = 'exchange/'
                destination_url = urlparse.urljoin(self.base_url(self.url), 'exchange/')
                form_url = destination_url
                post_data = None

        # Post the login form
        if not form_url:
            form_url = urlparse.urljoin(frontpage_url, form_path)
                            
        html = self.get_page(str(form_url), 
            post_data=post_data, set_cookies=True)
        
        if 'You could not be logged on to Outlook Web Access' in html:
            raise InvalidLogin
        if 'password that you entered is invalid' in html:
            raise InvalidLogin
        if 'is not valid' in html:
            raise InvalidLogin
            
        # We're good, find the base URL
        soup = BeautifulSoup(html) #, convertEntities=BeautifulSoup.HTML_ENTITIES)
        self._frontpage_soup = soup
                              
        if '(c) 2006 Microsoft Corporation' in html:
            self.version = '2007'
            self._frontpage_url = frontpage_url
            self._owa_url = re.sub(r'/\w+/?$', '/owa/', destination_url)
            self._auth_url = self._owa_url + 'auth/'
            
            log("_owa_url:", self._owa_url)
            log("_auth_url:", self._auth_url)

            self._owa2007_contacts = {}                     # map of IDs to tuples (name, email_address)
            self._owa2007_searchable_addressbooks = None    # list of IDs of address books where we can look up names 
        else:
            self.version = '2003'
            bases = soup.findAll('base')
            self._base_href = str(bases[0]['href'])
        self.server = True
    
               
    def logout(self):
        try:
            if self.version == '2003':
                url = self._base_href + '?Cmd=logoff'
            elif self.version == '2007':
                url = self._auth_url + 'logoff.aspx?Cmd=logoff'
            # Don't follow any redirects, set cookies, etc. after logging out,
            #  so we use raw_get_page rather than get_page
            errcode, response_headers, content = self.raw_get_page(url)
        except:
            pass
            
        self.server = False
        self.folder = None
        self._cookies.clear()
           
    def get_folder_list(self):
        
        log("OWA.get_folder_list")
        
        if not self.server:
            raise Exception("get_folder_list: must be logged in to do this")
                
        if self.version == '2003':
            # Returns only the first page of folder names (?)
            #  (Very few folks would have more than one page of these, not sure what OWA does)
            result = []
            url = self._base_href + '?Cmd=contents&ShowFolders=1'
            html = self.get_page(url, set_cookies=True)   
            soup = BeautifulSoup(html) #, convertEntities=BeautifulSoup.HTML_ENTITIES)
            folder_fields = soup.findAll(attrs={'name': 'FldID'})
            for f in folder_fields:
                name = urllib.unquote(f['value'].strip('/'))
                parent = ''
                log("%%%%%%%%", name)
                f = self.folder_priority(name, parent)
                if f is not None:
                    result.append(f)
                    
        elif self.version == '2007':
            
            # No new http request -- folder list is already in the front page
            # TODO: Full folder list, this is just the ones you see by default
            result = []
            folder_list = self._frontpage_soup.findAll("a", {"name": "lnkFldr"})
            for f in folder_list:
                name = str(f['title'])
                if 'IPF.Note' in f['href']:
                    m = re.search(r"id=(?P<owa_id>[^&]+)", f['href'])
                    if m:
                        owa_id = str(urllib2.unquote(m.groups()[0]))
                        parent = ''
                        log("%%%%%%%%", name)
                        
                        f = self.folder_priority(name, parent)
                        if f is not None:
                            f['owa_id'] = owa_id    # this is specific to OWA 2007
                            result.append(f)
                                
        else:
            raise Exception() 
        
        result.sort(key=lambda f: f['priority'])
        log("get_folder_list: returning these folders:", result)
        return result         
                         
    def _get_page_from_folder(self, folder, page=1):
        """
        Returns a list of tuples (msgid, datetime) for all messages
        since the given date (or since the msgid given in highest_path).
        List is sorted by date, newest first.
        """
        #log ("&&& ", folder, page)
             
        if self.version == '2003':
            url = self._base_href + urllib.quote(folder['name']) + '/?Cmd=contents&View=Messages&Page=%d&SortBy=Received&SortOrder=descending' % page
            html = self.get_page(url, set_cookies=True)
            soup = BeautifulSoup(html) #, convertEntities=BeautifulSoup.HTML_ENTITIES)
            
            # Get the total # of pages
            try:
                input = soup.findAll('input', attrs={'name': 'Page'})[0]
                tt = input.nextSibling.firstText().contents[0]
                m = re.match(r'\D*(\d*)', tt)
                num_pages = int(m.group(1))
            except:
                num_pages = 1
                
            # Get the message IDs
            inputs = soup.findAll("input", attrs={'name': 'MsgID'})
            result = []
            for i in inputs:
                msg_url = i['value']
                d = i.parent.parent.contents[-2].findAll(text=re.compile('\d*/\d*/\d*'))
                if not d:
                    d = i.parent.parent.contents[-1].findAll(text=re.compile('\d*/\d*/\d*'))
                if d:
                    try:
                        # TODO: Other Exchange date formats (it can be changed in the settings)
                        dt = self.tzinfo.localize(datetime.strptime(d[0], '%a %m/%d/%Y %I:%M %p'))
                    except:
                        dt = None
                else:
                    dt = None
                result.append((msg_url, dt))
                            
            return result, num_pages   
        
        elif self.version == '2007':
            url = self._owa_url + "?ae=Folder&t=IPF.Note&id=" + urllib.quote(folder['owa_id']) + "&slUsng=0&pg=" + str(page)
            html = self.get_page(url, set_cookies=True)
            soup = BeautifulSoup(html) #, convertEntities=BeautifulSoup.HTML_ENTITIES) 
            msgs = soup.findAll(onclick=re.compile(r"onClkRdMsg.*"))
            result = []
            for m in msgs:
                matches = re.search(r"'(?P<type>.+)',\s*(?P<index>\d+),\s*(?P<fc>\d+)\)", m['onclick'])
                if matches:
                    msg_type, msg_index, msg_fc = matches.groups()
                    try:
                        datestr =  m.parent.parent.parent.find("td", {"class": re.compile("^sc.*")}).contents[0]
                        # datestr will be something like u'5/4/2009&nbsp;9:22 AM&nbsp;'
                        datestr = datestr.replace('&nbsp;', ' ').strip()
                        dt = self.tzinfo.localize(datetime.strptime(datestr, '%m/%d/%Y %I:%M %p'))
                    except:
                        dt = None
                    try:
                        msg_url = str('%s,%s' % (msg_type, m.parent.parent.parent.find('input', {'name': 'chkmsg'})['value']))
                    except:
                        continue                # Can't fetch this message
                    result.append((msg_url, dt))
            
            # Get the total # of pages  
            num_pages = 1  
            page_tds = soup.findAll('td', {'class': 'pTxt'})
            for p in page_tds:
                try:
                    num_pages = max(num_pages, int(p.contents[0]))
                except:
                    try:
                        num_pages = max(num_pages, int(p.find('a').contents[0]))
                    except:
                        pass
            return result, num_pages
    
#        message_urls = re.findall(r'(?i)NAME=MsgID value="([^"]*)"', html)
#        return message_urls        
    
    def select_folder(self, folder, since=None, max_msgs=None, highest_path=None, highest_date=None, get_tuples=True):
        """
        Returns a list of tuples (msgid, datetime) for all messages
        since the given date (or since the msgid given in highest_path).
        List is sorted by date, oldest first.
        """
                
        if not self.server:
            raise Exception("select_folder: must be logged in to do this")
                    
        if self.version == '2007' and 'owa_id' not in folder:
            log("OWA 2007 requires full folder ID")
            raise Exception()
              
        page, num_pages, done, result = 0, 1, False, []
        while (page < num_pages) and not done:
            if (max_msgs is not None) and len(result) >= max_msgs:
                break
            
            page += 1
            
            try:
                page_result, num_pages = self._get_page_from_folder(folder, page=page)
                
                if not get_tuples:                  # Just need to get the first page to select the folder
                    return True
                
                if num_pages is None:
                    num_pages = 1
                num_pages = min(num_pages, 4)       # For now, this is the max # of pages we crawl back
            except:
                break
    
            # Only crawl back as far as the last message crawled, or 
            #  (initially) back a certain number of days
            for i in range(len(page_result)):
                msg_path, dt = page_result[i]
                if (highest_path is not None) and (msg_path == highest_path):
                    page_result, done = page_result[:i], True
                    break
                if (since is not None) and (dt is not None) and (dt < since):
                    page_result, done = page_result[:i], True
                    break  
                if (highest_date is not None) and (dt is not None) and (dt < highest_date):
                    page_result, done = page_result[:i], True
                    break                  
                
            result.extend(page_result)
            if not page_result:
                done = True
        
        result.reverse()                # Return them in ascending order by date
        if max_msgs is not None:        # List is in ascending order, so get the last <max_msgs>
            result = result[-max_msgs:]
          
        return result
        
        
    # Try to get the headers
    def _good_header_line(self, h):
        # Filter out the Microsoft envelope line, 
        #  the Python envelope line ("from " (note the space!!!)),
        #  blank lines,
        #  and filter out the content type because we're faking our own (text/html)
        for word in 'from ', 'microsoft', 'content-type', 'content-transfer-encoding', '--':
            if h.lower().startswith(word):
                return False
        if not h.strip():
            return False
        return True
        
    def fetch_raw_message(self, path):
        "Returns the raw e-mail for the given message path, without attachments."
        
        from email.mime.multipart import MIMEMultipart
        from email import Encoders
        from email.MIMEText import MIMEText
        from email.MIMEBase import MIMEBase
        
        path = str(path)        # Must not be unicode!
        if not self.server:
            raise Exception("fetch_raw_message: must be logged in to do this")
        
        if self.version == '2003':
            raw_message = self.get_page(str(self._base_href + path + '?Cmd=body'), headers=[('Translate', 'f')])
            flags = {}
            return raw_message, flags, []
        
        elif self.version == '2007':
                        
            # All we can do here is retrieve the message headers, and (separately) the HTML message. Sigh ...
            
            msg = MIMEMultipart()
            message_type, message_outlookid = path.split(',')
                    
            # Get the main page of the message first
            main_url = self._owa_url + '?ae=Item&t=' + message_type + '&id=' + urllib2.quote(message_outlookid)
            html_enclosure = self.get_page(main_url)
            main_soup = BeautifulSoup(html_enclosure)

            # ------ Step 1: Get, or reconstruct, the headers

            try:
                # TODO: Get the headers stuff working again [eg 8/5]
                # Use the secret OWA Premium interface to get message headers ... may or may not work!
#                header_url = self._owa_url + 'ev.owa?oeh=1&ns=ReadMessage&ev=LMD&Id=' + \
#                    urllib2.quote('id=' + urllib2.quote(message_outlookid))
#                header_enclosure = self.get_page(header_url)
#                header_soup = BeautifulSoup(header_enclosure)                
#                headers = header_soup.find('textarea', {'id': 'txtMailHdrs'}).contents[0].strip()

                raise NotImplemented
                # TODO
            
            except:
                # No headers (for example, Sent items) - have to resort to more screen-scraping
                               
                msghd = main_soup.find('table', {'class': 'msgHd'})
                fromm, to, cc, bcc, date_sent = ('', ''), [], [], [], now()
                
                # Subject
                try:
                    subject = decode_htmlentities(msghd.find('td', {'class': 'sub'}).contents[0]).strip()
                except:
                    subject = ''
                    
                # From                
                try:
                    id = msghd.find('td', {'class': 'frm'})
                    span = id.find('span')                        
                    fromm = self._get_owa2007_contact(span)
                except:
                    pass
                
                # Sent, To, Cc
                for hdr in msghd.findAll('td', {'class': 'hdtxt'}):
                    hdtxt = decode_htmlentities(hdr.contents[0]).strip()
                    if 'sent:' in hdtxt.lower():
                        try:
                            dt = hdr.parent.find('td', {'class': 'hdtxnr'}).contents[0].strip()
                            # Example dt: "Friday, June 05, 2009 1:12 AM"
                            date_sent = self.tzinfo.localize(datetime.strptime(dt, '%A, %B %d, %Y %I:%M %p'))
                        except:
                            log("exchange.OWA: Couldn't parse sent date ", dt)
                    elif 'to:' in hdtxt.lower():
                        for to_span in hdr.parent.findAll('span', {'class': 'rwRO'}):
                            to.append(self._get_owa2007_contact(to_span))
                    elif 'cc:' in hdtxt.lower():
                        for cc_span in hdr.parent.findAll('span', {'class': 'rwRO'}):
                            cc.append(self._get_owa2007_contact(cc_span))
                    elif 'bcc:' in hdtxt.lower():
                        for cc_span in hdr.parent.findAll('span', {'class': 'rwRO'}):
                            cc.append(self._get_owa2007_contact(bcc_span))                    
                    
                # Create fake headers        
                # Fake message ID ... hopefully our threading code is smart enough to deal with this
                from_domain = fromm[1].split('@')[-1] if fromm[1] else settings.NOTED_DOMAIN_MAIN
                message_id = '<%s.notedgen@%s>' % (hashlib.md5(path).hexdigest(), from_domain)
                msg['Message-ID'] = message_id
                msg['From'] = formataddr_unicode(fromm)
                msg['Date'] = formatdate_for_email(date_sent)
                if to:
                    msg['To'] = ', '.join([formataddr_unicode(x) for x in to])
                if cc:
                    msg['Cc'] = ', '.join([formataddr_unicode(x) for x in cc])
                msg['Subject'] = encode_header_unicode(subject)

            # ------ Step 2: Get the message body, attach it to the message
 
#            if "unicode" in subject.lower() or "strange" in subject.lower():
#                import pdb
#                pdb.set_trace()
            
            message_body, fragments = '', False
            for body_div in main_soup.findAll('div', {'class': 'BodyFragment'}):
                fragments = True
                message_body += body_div.renderContents()
            if not fragments:
                for body_div in main_soup.findAll('div', {'class': 'bdy'}):
                    message_body += body_div.renderContents() 
            if message_body.strip():
                message_body = \
                    '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">' + \
                    '<html><!-- ' + HTML_TEXT_OUTLOOK_CONVERSION_STRING + '--><body>' + \
                    message_body + '</body></html>'               
            
            if all(ord(c) < 128 for c in message_body):
                part = MIMEText(message_body, 'html')
            else:
                part = MIMEText(message_body, 'html', 'utf-8')
            msg.attach(part)
            
            # ------ Step 3: Get a list of attachments (but don't actually store them right now)
            filelist = []
            
            # No file attachments for OWA, until we solve the problem of attachments that are too big [eg 4/20/10]
#            atts = main_soup.findAll('a', {'id': 'lnkAtmt'})
#            
#            for a in atts:
#                            
#                att_url = urllib.basejoin(self._owa_url, a['href'])
#                att_name = a['title']
#                
#                import mimetypes
#                mimetype = mimetypes.guess_type(att_name)[0]
#                if not mimetype:
#                    mimetype = 'application/octet-stream'       # default mimetype
#                mimetype, mimesubtype = mimetype.split('/')
#
#                part = mimepart.OWAMIMEPartDescriptor(att_url, att_name, mimetype, mimesubtype)
#                filelist.append(part)
                
            raw_message = msg.as_string(unixfrom=False)                             
            flags = {}
                       
            return raw_message, flags, filelist 
                
    def fetch_raw_message_part(self, path, mimepath):
        """Returns a file attachment from the message.
        mimepath is just the direct URL of the file
        path (the path to the message itself) is ignored here
        """
                
        mimepath = str(mimepath)        # Must not be unicode!
        if not self.server:
            raise Exception("fetch_raw_message: must be logged in to do this")
        
        # Download the attachment
        errcode, response_headers, content = self.raw_get_page(mimepath)
        return content, False               # False means it's a raw binary blob
    
    def _decode_owa2007_contactstr(self, s):
        contact_str = decode_htmlentities(s).strip()
        m = re.match(r"(?P<name>.*)\[(?P<email>[^\[]+)\]$", contact_str)
        if m:          
            email_address = m.group('email').lower().strip()
            name = m.group('name').strip()
        else:
            m = RE_EMAIL_SEARCH.search(contact_str)
            if m:
                email_address = m.group()
                name = ''
            else:
                email_address = ''
                name = contact_str
        return name, email_address
    
    def _find_owa2007_contact_id(self, url, search_str):
        """Search for the given contact on the search form.
        This may be the Contact book, or one of the Address Books.
        Return an ID which can be looked up later to retrieve the email address of the contact."""
        
        search_page = self.get_page(url + '&sch=' + urllib2.quote(search_str))
        search_soup = BeautifulSoup(search_page)
        ids = search_soup.findAll('a', {'onclick': re.compile(r"return onClkRcpt.*")}) # use onclick instead of onClick
        if ids:
            return ids[0]['id']
        else:
            # No search results
            return None
                                                               
    def _get_owa2007_contact(self, span):
        a = span.find('a')
        
        email_address = ''
        if a and a.get('class', None) == 'emadr':       # Don't use " 'class' in a " expression, doesn't work!
                                                        #  (bug in BeautifulSoup?)
            name, email_address = self._decode_owa2007_contactstr(a.contents[0])
        elif not a:
            name, email_address = self._decode_owa2007_contactstr(span.contents[0])
            
        if not email_address:
            try:
                id = span.find('a')['id']
            except:
                if name:
                    try:
                        # id isn't given here -- have to search in some different places
                        search_str = name
                        if search_str:
                            # Search the Contacts first
                            id = self._find_owa2007_contact_id(self._owa_url + '?ae=Folder&t=IPF.Contact&newSch=1&scp=1', search_str)
                            
                            if not id:
                                for book in self._get_owa2007_searchable_addressbooks():
                                    id = self._find_owa2007_contact_id(self._owa_url + book, search_str)
                                    if id:
                                        break
                                    
                            if not id:
                                # No search results anywhere
                                return '', ''
                        else:
                            return '', ''
                    except:
                        return '', ''
                else:
                    return '', ''

            if id in self._owa2007_contacts:
                return self._owa2007_contacts[id]
            else:
                # Fetch the contact page for that contact
                
                try:
                    if '==' in id:
                        url = self._owa_url + '?ae=Item&t=AD.RecipientType.User&id=' + urllib2.quote(id) + '&m=0'
                    else:
                        url = self._owa_url + '?ae=Item&t=IPM.Contact&id=' + urllib2.quote(id) + '&m=0'
                    contact_page = self.get_page(url)
                    contact_soup = BeautifulSoup(contact_page)
                    name = decode_htmlentities(contact_soup.find('td', {'class': 'dn pLT'}).contents[0]).strip()                                
                    if '==' in id:
                        email_address = ''
                        
                        try:
                            email_address = decode_htmlentities(contact_soup.find('td', {'class': 'pLT pB'}).find('span').contents[0]).strip()
                            if not RE_EMAIL.match(email_address):
                                email_address = ''
                        except:
                            pass
                        
                        if not email_address:
                            for td in contact_soup.findAll('td', {'class': 'txvl'}):
                                email_address = decode_htmlentities(td.contents[0]).strip()
                                if RE_EMAIL.match(email_address):
                                    break
                                else:
                                    email_address = ''
                            
                    else:
                        email_address = decode_htmlentities(contact_soup.find('a', {'class': 'emadr'}).contents[0]).strip()
   
                except:
                    name, email_address = '', ''
                    
                self._owa2007_contacts[id] = (name, email_address)
        return (name, email_address)


    def _get_owa2007_searchable_addressbooks(self):
        """Return a cached list of the address books where we might try to find an email address by name
        (if we can't get the email address any other way). For now, only use Global Address List address books."""
        
        if self._owa2007_searchable_addressbooks is None:
            
            self._owa2007_searchable_addressbooks = []
            
            try:
                # Go to the main Address Book page, get the list of individual address books
                url = self._owa_url + '?ae=Dialog&t=AddressBook&ctx=1'
                page = self.get_page(url)
                soup = BeautifulSoup(page)
            
                select = soup.find('select', {'id': 'selSch'})
                if select:
                    for option in select.findAll('option'):
                        name = option.contents[0].strip()
                        value = option['value']
                        if re.search(r'(?i)(global|address ?book)', name):
                            self._owa2007_searchable_addressbooks.append(value)
                        
            except:
                pass
        
        return self._owa2007_searchable_addressbooks
        
        


    
