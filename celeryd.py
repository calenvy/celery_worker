# Simple launcher for celeryd, makes sure that the celeryconfig.py file can be found

from celery.bin.celeryd import run_worker, OPTION_LIST
import sys, os

PROJECT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.insert(0, PROJECT_DIR)      # make sure Celery can find the celeryconfig.py file

print "Starting celeryd..."
run_worker()
