import sys,os
from os.path import *
project_root = dirname(abspath(__file__))
server_indicator_file = dirname(project_root) + os.sep + "server_type"

if exists(server_indicator_file):
    fh = open(server_indicator_file)
    server_type = fh.readline().strip()
    fh.close()
else:
    server_type = "local"
    
if server_type not in ["local", "staging", "live"]:
    print "Server type %s unrecognised." % server_type
    sys.exit(1)
